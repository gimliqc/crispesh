
-- INSERTION ACCUEIL

insert into ACCUEIL(SLOGAN, TEXTE, MISSION_A, VALUES_A, VISION_A) values
('									<p>INNOVER POUR FAVORISER LA PARTICIPATION SOCIALE</p>
									<p>DES PERSONNES EN SITUATION DE HANDICAP</p>'
,'<br>
                        <p><br/>Le Centre de recherche pour l’inclusion des personnes en situation de handicap (CRISPESH) est un centre collégial de transfert de technologie en pratiques sociales novatrices (CCTT-PSN), né d’un partenariat entre le <a href="http://www.cvm.qc.ca/Pages/index.aspx" target="_blank">Cégep du Vieux Montréal;</a>et le <a href="http://www.dawsoncollege.qc.ca/french/" target="_blank">Collège Dawson</a>. Le Centre a été reconnu en octobre 2010 par le <a href="http://www.education.gouv.qc.ca/" target="_blank">Ministère de l’Éducation et de l’Enseignement Supérieur.</a></p>
                        <p>En juillet 2015, le cégep du Vieux Montréal et le collège Dawson confiaient la gestion du CRISPESH à un organisme<br> à but non lucratif du même nom créé dans le but de réaliser le mandat du Centre.</p>
                        <center>
							<img src="images/logo.png" alt="">
						</center>
						<br>
						<p>Le CRISPESH fait partie du <a href="http://reseautranstech.qc.ca/" target="_blank">Réseau Trans-tech</a>, un regroupement de 49 centres collégiaux de transfert de technologie réunissant plus de 1000 experts à travers le Québec qui se dédient à l’innovation au moyen de la recherche appliquée et du transfert.</p>
						<br>'
,'<p>Le CRISPESH a plus spécifiquement pour mission l’avancement des connaissances ainsi que le développement et la promotion de pratiques sociales favorisant l’inclusion scolaire, sociale et professionnelle des personnes vivant une situation de handicap.</p>'
,'<p>En faisant de l’inclusion le cœur de sa mission, le CRISPESH est un fier défenseur de l’égalité des chances pour tous. Vecteur de rigueur intellectuelle et d’innovation, il réalise sa mission en valorisant le respect, l’engagement et la collaboration afin de sensibiliser et responsabiliser la société québécoise à la diversité et à l’importance de créer et de mettre en place des initiatives durables qui contribuent à la pleine inclusion de personnes en situation de handicap.</p>'
,'<p>En réunissant des leaders et des acteurs issus des domaines du développement, de la recherche, du transfert et de l’innovation, le CRISPESH compte ouvrir la voie vers la pleine inclusion des personnes en situation de handicap, et ce, dans tous les secteurs de la société.</p>');


-- INSERTION DES MEMBRES

insert into MEMBRE(EMAIL, PRENOM, NOM, ROLE_ACTUEL, CATEGORIE, TELEPHONE, POSTE) values
('rducharme@cvm.qc.ca', 'Roch', 'Ducharme', 'Directeur général','personnel administratif','514 982-3437', 'poste 2835');

insert into MEMBRE(EMAIL, PRENOM, NOM, ROLE_ACTUEL, CATEGORIE, TELEPHONE, POSTE) values
('jywang@cvm.qc.ca', 'Jian', 'Wang', 'Adjointe administrative','personnel administratif','514 982-3437', 'poste 2836');

insert into MEMBRE(EMAIL, PRENOM, NOM, ROLE_ACTUEL, CATEGORIE) values
('abigras@cvm.qc.ca', 'Audrey', 'Bigras', 'Professionnelle de recherche', 'personnel scientifique');

insert into MEMBRE(EMAIL, PRENOM, NOM, ROLE_ACTUEL, CATEGORIE) values
('smcinnis@cvm.qc.ca', 'Shannahn', 'McInnis', 'Chargée de projet', 'personnel scientifique');

insert into MEMBRE(EMAIL, PRENOM, NOM, ROLE_ACTUEL, CATEGORIE) values
('llefevreRadelli@cvm.qc.ca', 'Léa', 'Lefevre-Radelli', 'Coordonnatrice de projet et Assistante de recherche', 'personnel scientifique');

insert into MEMBRE(EMAIL, PRENOM, NOM, ROLE_ACTUEL, CATEGORIE) values
('pturcotte@cvm.qc.ca', 'Paul', 'Turcotte', 'Enseignant au Cégep du Vieux Montréal', 'personnel scientifique');

insert into MEMBRE(EMAIL, PRENOM, NOM, ROLE_ACTUEL, CATEGORIE) values
('jdarrous@dawsoncollege.qc.ca', 'Joseph', 'Darrous', 'Technicien en éducation spécialisée au Collège Dawson', 'personnel scientifique');

insert into MEMBRE(PRENOM, NOM, ROLE_ACTUEL, CATEGORIE) values
('Rola', 'Helou', 'Directrice du Conseil scolaire des Premières Nations en éducation aux adultes', 'personnel scientifique');
insert into MEMBRE(PRENOM, NOM, ROLE_ACTUEL, CATEGORIE) values
('Amanda', 'Shawayahamish', 'Étudiante à l''université Concordia', 'étudiants');

insert into MEMBRE(PRENOM, NOM, ROLE_ACTUEL, CATEGORIE) values
('Celeste', 'Awashish', 'Étudiante au Collège Dawson', 'étudiants');

insert into MEMBRE(PRENOM, NOM, ROLE_ACTUEL, CATEGORIE, ROLE_1, EMPLACEMENT_1, ROLE_2) values
('Catherine', 'Fichten', 'Enseignante au Collège Dawson', 'chercheurs', 'Professeure associée à l’Université McGill
', 'Département de psychologie','Codirectrice du Réseau de Recherche Adaptech (www.adaptech.org)');

insert into MEMBRE(PRENOM, NOM, ROLE_ACTUEL, CATEGORIE, ROLE_1, ROLE_2) values
('Alice', 'Havel', 'Retraitée du Collège Dawson', 'chercheurs', 'Coordonnatrice du AccessAbility Centre', 'Chercheuse associée au Réseau de Recherche Adaptech');

insert into MEMBRE(PRENOM, NOM, ROLE_ACTUEL, CATEGORIE, ROLE_1) values
('Tara', 'Flanagan', 'Professeure Associée à l’Université McGill', 'chercheurs', 'Département d’Educational and Counselling Psychology');

insert into MEMBRE(PRENOM, NOM, ROLE_1, CATEGORIE, EMPLACEMENT_1) values
('Josianne', 'Robert', 'Professeure de formation pratique adjointe à l’Université de Montréal', 'chercheurs', 'Département d’Educational and Counselling Psychology');

insert into MEMBRE(PRENOM, NOM, ROLE_ACTUEL, CATEGORIE, ROLE_1) values
('Richard', 'Filion', 'Président', 'Conseil', 'Directeur général du Collège Dawson');

insert into MEMBRE(PRENOM, NOM, ROLE_ACTUEL, CATEGORIE, ROLE_1) values
('Mylène', 'Boisclair', 'Vice-présidente', 'Conseil', 'Directrice générale au Cégep du Vieux Montréal');

insert into MEMBRE(PRENOM, NOM, EMPLACEMENT_1, CATEGORIE, ROLE_1) values
('Martin', 'Prévost', 'Secrétaire et trésorier', 'Conseil', 'Directeur adjoint des études au Cégep du Vieux Montréal');

-- INSERTION DES PROJETS


insert into PROJET(COMPLETED,CATEGORIE, TITRE, IMAGE_P, DESCRIPTION_P, ROLE_1, PERSONNE_1, ROLE_2, PERSONNE_2, PARTENAIRE_1, PARTENAIRE_2, PARTENAIRE_FIN_1) values
('non','photographie expression','La photographie comme moyen d’expression, de participation sociale, d’autodétermination et d’inclusion des étudiants en situation de handicap au collégial (2015-2016)', 'images/photographe.jpg', '<p>Grâce à une subvention de l’Office des personnes handicapées du Québec (OPHQ) le CRISPESH favorise la participation sociale des étudiants en situation de handicap du collégial. En utilisant la photographie comme moyen d’expression, les étudiants qui participent au projet ont l’occasion de partager leurs perspectives sur leur expérience au sein de la communauté collégiale.</p>
                    <p>Les cadres autour desquels s’articule ce projet sont l’autodétermination et la photographie participative. En favorisant le développement de l’autodétermination, ce projet permet à chacun des participants d’être le principal agent d’amélioration de sa qualité de vie et d’avoir un impact positif sur son milieu d’études. La photographie participative génère quant à elle un pouvoir d’expression par l’image qui se fonde sur l’idée que toute personne est la mieux placée pour communiquer son point de vue à une communauté.</p>
					<p>Divisé en deux volets distincts (Création et Sensibilisation), ce projet se déroule tout au long de l’année 2016. Le volet Création, qui a eu lieu durant la session d’hiver 2016, vise à ce que les étudiants développent une perspective critique de leur propre expérience et qu’ils soient en mesure d’extérioriser un point de vue personnel en utilisant une démarche de création. Le volet Sensibilisation, qui a lieu au cours de la session d’automne 2016, a pour objectif le partage du point de vue développé, afin de sensibiliser la communauté collégiale à la diversité des expériences et des perspectives sur le handicap.</p>
					<p>Tout au long de ce projet, les étudiants sont accompagnés par un photographe professionnel qui les soutient dans la démarche de création et la réalisation des aspects techniques de la production des images.  L’offre du support technique nécessaire à la réalisation de l’œuvre constitue une force de cette approche. Ce soutien technique réduit considérablement le risque que le créateur échoue suite à un trop grand écart séparant l’idée initiale du résultat final. De cette façon, les étudiants pourront s’exprimer librement tout en bénéficiant du large potentiel créatif de la photographie.</p>'
          , 'Chargée de projet', 'Marise Lachapelle'
          , 'Coordonnatrice des activités', 'Jian Wang'
          , 'Centre collégial de soutien à l’intégration (CCSI) de l’Ouest'
          , 'L’association québécoise des étudiants ayant des incapacités au postsecondaire (AQEIPS)'
          , 'Office des personnes handicapées du Québec');

insert into PROJET(COMPLETED,CATEGORIE, TITRE, IMAGE_P, DESCRIPTION_P, ROLE_1, PERSONNE_1, ROLE_2, PERSONNE_2, ROLE_3, PERSONNE_3, PARTENAIRE_1, ORGANISME_SUBV) values
('non','Soutenir employeur','Soutenir l’employeur dans l’inclusion professionnelle des personnes vivant avec une déficience intellectuelle ou un trouble du spectre de l’autisme (2016-2017)','images/cle.jpg', '<p>Le Centre intégré de santé et de services sociaux de la Montérégie-Ouest (CISSSMO) offre des services d’adaptation, de réadaptation et d’intégration sociale aux personnes qui vivent avec une déficience intellectuelle (DI) ou un trouble du spectre de l’autisme (TSA). Pour ce faire, il leur fournit une gamme de services de soutien et d’accompagnement. Dans un souci d’offrir des services basés sur des données probantes et de faciliter l’implantation de meilleures pratiques, le CISSSMO a remis à jour son programme d’intégration et de réadaptation au travail. Afin de permettre à ses usagers de vivre une intégration au travail positive et valorisante, le CISSSMO constate que le soutien à l’employeur est primordial. Toutefois, la force de son expertise réside davantage dans l’identification des besoins et le soutien des personnes vivant avec une DI ou un TSA. Pour remédier à la situation, le CISSSMO bénéficiera de l’expertise du Centre de recherche pour l’inclusion scolaire et professionnelle des étudiants en situation de handicap (CRISPESH) afin de mieux répondre aux besoins des employeurs avec lesquels il collabore. Cela facilitera la mise en place d’environnements de travail inclusifs où les contextes qui mettraient leurs usagers en situation de handicap seraient réduits. L’obligation légale d’égalité à l’emploi au Québec se fonde sur le constat d’une discrimination systémique à l’endroit de certaines personnes, dont les personnes en situation de handicap (PSH). Plusieurs employeurs croient que celles-ci sont moins productives, ce qui engendre des pratiques discriminatoires. Pour contrer cette tendance, le monde de la gestion s’est donné un cadre d’opération vers un engagement planifié et systématique à recruter et maintenir en emploi des personnes ayant des profils (expériences, habiletés, etc.) diversifiés. Sous la bannière du concept de « gestion de la diversité », ce cadre vise l’équité et l’égalité des chances dans le monde du travail. Il s’agit de valoriser et reconnaître les différences individuelles en partant du postulat que chaque personne a le potentiel de contribuer à la performance d’une entreprise. Toutefois, ce cadre ne s’est pas encore généralisé dans les pratiques et au Québec, il comprend rarement les PSH.</p>
                    <p>Dans le cadre de ce projet, le CRISPESH et le CISSSMO uniront leurs efforts et leurs expertises pour développer des stratégies visant à favoriser la mise en place de pratiques de gestion de la diversité inclusive pour les PSH. En s’inspirant du concept d’inclusion, tel que défini en éducation, et du domaine de la gestion de la diversité, ce projet a pour objectif général le développement de bonnes pratiques de préparation et de soutien aux employeurs vers une gestion plus inclusive des personnes vivant avec une DI ou un TSA dans les milieux de travail. Les objectifs particuliers visent à élargir et à consolider les connaissances en matière de gestion de la diversité, de leadership d’inclusion et de changements de pratiques de gestion, en portant une attention particulière au handicap. Aussi, les chercheurs documenteront le point de vue de l’employeur sur les pratiques d’inclusion et d’intégration des personnes vivant avec une DI ou un TSA. Ce projet est novateur, puisqu’il compte développer un cadre de gestion de la diversité au Québec qui s’intéresse spécifiquement au handicap.</p>
                    <p>De façon générale, ce projet favorisera l’augmentation de la participation sociale des PSH grâce à une plus grande inclusion dans le monde professionnel. Il permettra au CISSSMO de mieux soutenir les employeurs avec lesquels il collabore, en plus de faciliter l’intégration des usagers. Les collèges et le CRISPESH innoveront et renforceront leur capacité de recherche et de transfert des connaissances grâce à une nouvelle expertise.</p>'
                    ,'Chargée de projet et chercheuse', 'Marise Lachapelle', 'Chercheur','Jean-Sébastien Goulet','Assistante de recherche - Étudiante en Techniques de gestion de commerces au Cégep du Vieux Montréal', 'Aurélie Angrignon Atkins'
                    ,'Centre intégré de santé et de services sociaux de la Montérégie-Ouest (CISSSMO)'
                    ,'Ministère de l’éducation et de l’enseignement supérieur');

insert into PROJET(COMPLETED,CATEGORIE, TITRE, IMAGE_P, DESCRIPTION_P, ROLE_1, PERSONNE_1, ROLE_2, PERSONNE_2, ROLE_3, PERSONNE_3, PERSONNE_4, PERSONNE_5) values
('oui','Amelioration francais'
,'L’amélioration du français écrit des adultes ayant un trouble d’apprentissage, soutenus par les aides technologiques (2012-2014)'
,'images/ipad.jpg'
, '<p>La problématique à l’origine de cette recherche est liée à la croissance de l’utilisation des aides technologiques pour améliorer la qualité du français écrit des adultes ayant un trouble d’apprentissage. L’omniprésence de l’écriture dans notre société actuelle justifie d’ailleurs cet intérêt marqué. Le problème, par contre, est que cette utilisation est peu documentée et peu évaluée. De plus, jusqu’à ce jour, aucune recherche n’a évalué l’effet de l’accompagnement dans l’utilisation des aides technologiques dans une perspective d’amélioration de l’écriture.</p>
                    <p>Donc, présentement, on offre différents services et fonctions d’aide de logiciels sans vraiment connaître l’impact réel des technologies sur le processus d’écriture ou même sur la qualité du français écrit. C’est pour pallier cette situation que, dans le cadre de cette recherche, un devis à cas unique, pour cinq sujets, a été retenu afin de permettre l’évaluation à la fois de l’effet de l’utilisation de certaines fonctions d’aide à l’écriture (édition de texte, dictionnaires, révision-correction et rétroaction vocale) et l’effet de l’accompagnement. La volonté d’élaborer un guide d’accompagnement dans l’utilisation des aides technologiques auprès des adultes ayant un trouble d’apprentissage soutenait également ce choix.</p>
                    <p>Les sujets ont donc été accompagnés dans leur processus d’écriture avec aides technologiques pendant 15 semaines à raison d’une fois par semaine. Tout au long de l’expérimentation, ceux-ci ont dû produire une rédaction hebdomadaire, ce qui nous a permis d’obtenir des données. L’analyse des résultats a permis de tirer un certain nombre de recommandations permettant de moduler l’accompagnement dans les aides technologiques afin  qu’il soit plus efficace. Ainsi, il est permis de recommander l’utilisation des fonctions Édition de texte et Révision-correction à tout adulte ayant un trouble d’apprentissage à la condition essentielle qu’y soit associé un accompagnement sur la technique du logiciel ainsi que sur le développement de stratégies d’écriture intégrant les différentes fonctions d’aide. En contrepartie, la prudence est de mise  en ce qui concerne l’utilisation des fonctions Dictionnaires et Rétroaction vocale puisque la recherche a aussi mis en lumière l’efficacité moins constante de ces outils. Il devient donc nécessaire de s’assurer que l’utilisateur répond bien à cette mesure d’aide et qu’il y a effectivement une amélioration de la qualité de son français écrit avant de lui en permettre une utilisation autonome. L’encadrement est essentiel. Le transfert de ces résultats au partenaire, l’Institut des troubles d’apprentissage (ITA), s’est fait et se poursuivra par une collaboration entre les personnes ressources de l’organisme et les chercheurs. En dernier lieu, il apparaît important de poursuivre la recherche dans ce domaine encore peu exploré puisqu’un trop grand décalage subsiste entre la progression de la demande d’utilisation des aides technologiques et le développement des connaissances sur le sujet.</p>'
,'Chercheuse - Enseignante au Cégep du Vieux Montréal - Département de français', 'Evelyne Pitre'
,'Chercheur - Fondateur et Coordonnateur, Solutions aides technologiques', 'Marc Tremblay'
, 'Assistants de recherche','Alexandra Cloutier', 'Mathieu Lauzon-Dicso', 'Camille Raunet');


insert into PROJET(COMPLETED,CATEGORIE, TITRE, IMAGE_P, DESCRIPTION_P, ROLE_1, PERSONNE_1, ROLE_2, PERSONNE_2, ROLE_3, PERSONNE_3, ROLE_4, PERSONNE_4, ROLE_5, PERSONNE_5, ROLE_6, PERSONNE_6, ROLE_7, PERSONNE_7, PARTENAIRE_1, ORGANISME_SUBV) values
('non', 'Soutien etudiant handicap'
,'Soutien à l’intégration des étudiants en situation de handicap: développement d’un instrument d’évaluation des besoins de l’apprenant (2015-2017)'
,'images/filles.jpg'
,'<p>L’objectif de ce projet est de développer un instrument d’évaluation à l’intention des intervenants des services adaptés pour soutenir les étudiants en situation de handicap dans leur réussite éducative. Le nombre d’étudiants en situation de handicap (ESH) au collégial s’est accru de manière remarquable au cours de la dernière décennie et leur profil s’est largement transformé. Les collèges disposent de services adaptés (SA) qui ont pour mandat l’accompagnement des ESH vers la réussite éducative. Ces SA ont démontré, depuis leur implantation, qu’ils favorisent la réussite des ESH qui les utilisent. Or, l’accompagnement des ESH se complexifie. Le constat actuel sur les pratiques d’accompagnement basées sur le modèle médical qui prévaut au sein des SA des collèges permet de conclure que ces pratiques ne sont pas à la hauteur des défis d’aujourd’hui. Le manque d’outils d’évaluation et de suivi des ESH est établi et met en évidence la limite actuelle des pratiques en accompagnement vers la réussite des ESH. Ce projet permettra, à terme, de dépasser cette limite.</p>
					<p>Au  terme de ce projet, nous développerons un instrument d’évaluation du profil d’apprenant des ESH en milieu collégial qui, en plus de générer un profil personnalisé, proposera diverses ressources (humaines et matérielles) encourageant la réussite éducative des ESH. De plus, cet outil de travail permettra le développement de comportements d’autodétermination chez les ESH, ce qui favorisa également leur réussite éducative. En plus d’offrir des réponses aux besoins des ESH de manière plus pointue et systématique, cet outil servira aux conseillers en services adaptés de tous les établissements de niveau collégial (privés et publics) de la province et favorisera une harmonisation des pratiques en la matière.</p>
					<p>La réussite éducative des étudiants est au cœur de la mission et des préoccupations de tous les établissements de niveau collégial de la province. Ce projet revêt une importance capitale pour le partenaire (CCSI), les collèges du Québec et leurs différents acteurs (personnel de direction, conseillers en services adaptés, aides pédagogiques individuels, enseignants, ESH), mais aussi la société dans son ensemble. En effet, une plus grande réussite éducative des ESH est un facteur déterminant d’une meilleure intégration future au marché de l’emploi et, ainsi, à la pleine valorisation et participation des ESH au sein de la société.</p>'
,'Chargée de projet et chercheuse','Marise Lachapelle	'
,'Professionnelle de recherche','Danièle Paquet	'
, 'Chercheur','Paul Turcotte'
,'Chercheuse associée','Josianne Robert'
,'Chercheuse (2015-2016)', 'Christine Morin'
, 'Assistante de recherche - Étudiante en Histoire et civilisation au Cégep du Vieux Montréal', 'Mélissa Miller'
, 'Assistante de recherche - Étudiante en Histoire et civilisation au Cégep du Vieux Montréal', 'Odile Trudeau-Richard'
, 'Centre collégial de soutien à l’intégration (CCSI)'
, 'Ministère de l’éducation et de l’enseignement supérieur');


insert into PROJET(COMPLETED,CATEGORIE, TITRE, IMAGE_P, DESCRIPTION_P,  ROLE_1, PERSONNE_1, ROLE_2, PERSONNE_2, ROLE_3, PERSONNE_3, ROLE_4, PERSONNE_4, ROLE_5, PERSONNE_5, ROLE_6, PERSONNE_6, ORGANISME_SUBV) values
('non','Ameliorer sommeil', 'Améliorer les conditions de sommeil de l’enfant vivant avec un trouble du spectre de l’autisme  grâce à un design réfléchi et personnalisé de sa chambre (2016-2017)'
,'images/ours.jpg'
, ' <p>La Fédération québécoise de l’autisme estime la prévalence du trouble du spectre de l’autisme (TSA) à près de 1% de la population canadienne, mais des recherches démontrent que cette donnée sous-évalue le nombre réel d’individus touchés par ce trouble. Les problèmes de sommeil sont fréquents chez les enfants qui ont un TSA. Les raisons de ces problèmes de sommeil sont variées et peuvent être, par exemple, d’ordre physiologique, cognitif, sensoriel ou une combinaison de facteurs. Quelles qu’en soient les causes, le manque de sommeil aura inévitablement des conséquences sur la qualité de vie de l’enfant et de sa famille. Bien que l’aménagement de la chambre figure au premier rang des solutions que les parents peuvent apporter, la chambre des enfants ayant un TSA demeure un lieu bien peu étudié en design d’intérieur. De plus, il est démontré que le TSA se manifeste par des réactions inhabituelles à des expériences sensorielles. En effet, les personnes touchées par le TSA ont souvent de la difficulté à traiter et à répondre aux différents stimuli sensoriels, tels que l’ouïe, la vue et le toucher. Elles peuvent être hypo ou hypersensibles. Plusieurs chercheurs suggèrent que l’hypersensibilité peut provoquer des réactions sensorielles à l’origine des difficultés de sommeil. Pourtant, bien que le risque que le sommeil soit perturbé pour des raisons sensorielles chez les enfants ayant un TSA soit particulièrement élevé, cette composante est peu documentée. Ce projet propose de s’intéresser à l’aménagement de la chambre de l’enfant de 2 à 12 ans ayant un TSA en se basant sur les perceptions sensorielles de celui-ci. Pour ce faire, le projet fera le pont entre les études sur l’aménagement d’environnements conviviaux pour les personnes ayant un TSA et transférera ces connaissances vers l’aménagement de la chambre.</p>
                    <p>L’objectif général de ce projet est de développer un cadre général, adaptable aux besoins individuels, d’aménagement de chambre pour enfants  de 2 à 12 ans vivant avec un TSA, qui se base sur les perceptions sensorielles. Les objectifs particuliers sont : 1) consolider et répertorier les connaissances sur les besoins sensoriels des enfants vivant avec un TSA (ex : toucher, sentir, goûter, voir, entendre, mouvement); 2) établir les paramètres à prendre en considération pour un design réfléchi et personnalisé; 3) proposer un ensemble flexible de directives qui puisse guider vers une réponse personnalisée et basée sur les profils de perception sensorielle (hypo/hypersensibilité).</p>
					<p>Grâce à la réalisation de ce projet, le CRISPESH pourra consolider ses connaissances et développer un créneau d’expertise peu étudié. Les programmes de Techniques de design d’intérieur du Cégep du Vieux Montréal et du Collège Dawson pourront bénéficier de nouveaux apprentissages visant à concevoir des aménagements réfléchis et personnalisés. Cela permettra au CRISPESH et à ses deux collèges affiliés de développer davantage leur capacité de recherche. L’originalité et la pertinence de ce projet résident dans une consolidation de connaissances qui n’ont jamais été regroupées à cette fin, mais aussi dans la prise en compte de l’expérience des personnes et des milieux concernés. Aucun effort n’a été déployé auparavant pour réunir toutes ces connaissances en vue d’apporter du soutien dans ce grand défi qui peut paraître banal pour toute personne qui n’est pas familière avec le sujet. Les ergothérapeutes peuvent certes aider dans cette entreprise et les informations obtenues grâce à l’évaluation sensorielle des enfants sont également utiles, mais aucun cadre de référence d’aménagement de la chambre de l’enfant ayant un trouble du spectre de l’autisme n’existe à l’heure actuelle. De façon générale, ce projet pourra améliorer la qualité de vie de plusieurs enfants et de leurs familles en contribuant à satisfaire ce besoin vital qu’est le sommeil.</p>'
,'Chargée de projet et chercheuse','Marise Lachapelle'
,'Professionnelle de recherche et coordonnatrice des activités','Audrey Bigras'
,'Chercheuse','Suzanne Pitre'
,'Chercheur','Joseph Darrous'
,'Assistante de recherche - Étudiante en Techniques de design d’intérieur au Cégep du Vieux Montréal','Charlotte Leroux'
,'Assistante de recherche - Étudiante en Techniques de design d’intérieur au Cégep du Vieux Montréal','Tess Perron-Laurin'
,'Ministère de l’éducation et de l’enseignement supérieur');

insert into PROJET(COMPLETED,CATEGORIE, TITRE, IMAGE_P, DESCRIPTION_P, ROLE_1, PERSONNE_1, ROLE_2, PERSONNE_2,ROLE_3, PERSONNE_3, ROLE_4, PERSONNE_4, PARTENAIRE_1, PARTENAIRE_2, PARTENAIRE_3, PARTENAIRE_4, PARTENAIRE_5, PARTENAIRE_6, PARTENAIRE_7,ORGANISME_SUBV) values
('non','Incubateur autochtone'
,'Développement d’un modèle d’incubateur d’entreprises inclusif destiné aux Autochtones (2016-2018)'
,'images/iphone.jpg'
,'<p>Les inégalités salariales et d’accès à l’emploi persistent entre les Autochtones et le reste de la population canadienne. Par ailleurs, les membres des Premières Nations qui se retrouvent en situation de handicap (troubles de santé mentale, handicap physique, sensoriel ou moteur, trouble ou difficulté d’apprentissage, etc.) peuvent facilement être sujets à une double discrimination sur le marché de l’emploi, de par leur appartenance culturelle et leurs besoins particuliers. Ces facteurs freinent leur épanouissement professionnel et empêchent le marché du travail de bénéficier de travailleurs qualifiés qui pourraient contribuer au maintien d’une économie locale prospère.</p>
                    <p>L’entrepreneuriat représente une voie qui peut faciliter l’inclusion sociale et économique de personnes marginalisées en misant sur leurs intérêts et leurs compétences. Cette avenue est flexible, facilement adaptable aux besoins particuliers des individus qui s’y engagent et respectueuse des conceptions sociales et culturelles spécifiques de l’économie locale. Toutefois, du soutien et de l’accompagnement sont essentiels afin de rendre cette voie possible et durable. En ce sens, l’incubateur d’entreprises est une structure d’accompagnement adaptée qui offre différents services afin de faciliter le passage de l’idée à la mise en place d’un projet d’entreprise.</p>
                    <p>Ce projet permettra de développer un modèle d’incubateur d’entreprises inclusif qui répondrait aux besoins et aux aspirations des membres des Premières Nations du Québec. Celui-ci sera inclusif puisqu’il sera adapté d’emblée pour les personnes en situation de handicap.</p>
					<p>Pour ce faire, quatre étapes seront nécessaires :</p>
					<ul>
						<li>Répertorier les modèles d’incubateurs d’entreprises et la littérature sur leurs succès et leurs échecs</li>
						<li>Documenter les perspectives spécifiques des populations autochtones en matière d’entrepreneuriat et de développement économique et social</li>
						<li>Concevoir un modèle d’incubateur d’entreprises inclusif prêt à être mis en fonction</li>
						<li>Développer les outils nécessaires à l’opérationnalisation de l’incubateur</li>
					</ul>	'
,'Directrice de projet','Marise Lachapelle','Coordonnatrice de projet et Assistante de recherche','Léa Lefevre-Radelli'
,'Chercheur','Paul Turcotte','Chercheuse','Rola Helou'
,'Conseil scolaire des Premières Nations en éducation aux adultes (CSPNEA)'
,'Commission de développement des ressources humaines des Premières Nations du Québec (CDRHPNQ)'
,'Commission de développement économique des Premières Nations du Québec et du Labrador (CDEPNQL)'
,'Tewatonhnhi’saktha'
,'Fondation Martin'
,'Cégep du Vieux Montréal'
, 'Collège Dawson'
,'Conseil de recherches en sciences humaines du Canada (CRSH)');

insert into PROJET(COMPLETED,CATEGORIE, TITRE, IMAGE_P, DESCRIPTION_P, ROLE_1, PERSONNE_1, ROLE_2, PERSONNE_2, ROLE_3, PERSONNE_3, ROLE_4, PERSONNE_4) values
('oui','Conception vetement'
,'Conception d’un vêtement de compression adapté pour des adolescents et de jeunes adultes vivant avec un trouble du spectre de l’autisme (2015-2016)'
,'images/vestes.jpg'
,'<p>Dans le cadre du Programme d’aide à la recherche et au transfert (PART), Vestechpro et le CRISPESH se sont associées à une entreprise québécoise pour concevoir un vêtement de compression adapté pour les adolescents et les jeunes adultes atteints d’un trouble du spectre de l’autisme. En effet, les vêtements disponibles à l’heure actuelle ne répondent pas aux besoins de cette clientèle. Les clients trouvent que ces vêtements sont volumineux, coûtent cher, ne sont pas totalement confortables et sont rarement ajustables au besoin de chacun en matière de pression ou compression. De plus, les produits disponibles visent surtout les jeunes enfants et permettent souvent d’identifier les personnes les portant comme étant différentes en raison de leur style particulier.</p>
                    <p>Ainsi, l’objectif principal de ce projet était la conception d’un vêtement adapté pour les adolescents et les jeunes adultes atteints d’un trouble du spectre de l’autisme.</p>'
,'Chargée de projet et chercheuse', 'Marise Lachapelle'
,'Chercheur','Joseph Darrous'
,'Assistante de recherche - Étudiante en Histoire et civilisation au Cégep du Vieux Montréal', 'Mélissa Miller'
,'Assistante de recherche - Étudiante en Histoire et civilisation au Cégep du Vieux Montréal', 'Odile Trudeau-Richard');

insert into PROJET(COMPLETED,CATEGORIE, TITRE, IMAGE_P, DESCRIPTION_P, ROLE_1, PERSONNE_1) values
('oui','Consortium diversité'
,'Consortium de recherche sur la gestion de la diversité en emploi (2014-2015)'
,'images/mosaic.jpg'
,'<p>Ce projet a regroupé trois centres collégiaux de transfert de technologie en pratiques sociales novatrices (CCTT-PSN) en vue de promouvoir et de favoriser l’inclusion professionnelle des jeunes âgés entre 25 et 34 ans de divers horizons, dont les immigrants et les personnes en situation de handicap. Le CRISPESH, le Centre d’études des conditions de vie et des besoins de la population (ÉCOBES) et l’Institut de recherche sur l’intégration professionnelle des immigrants (IRIPI) ont uni leurs expertises dans le cadre de cette alliance stratégique qui, à terme, a proposé une offre de service visant à améliorer le taux d’emploi de cette population et à répondre aux besoins de main-d’œuvre des entreprises québécoises.</p>	'
,'Chercheuse'
,'Marise Lachapelle');

insert into PROJET(COMPLETED,CATEGORIE, TITRE, IMAGE_P, DESCRIPTION_P, ROLE_1, PERSONNE_1, ROLE_2, PERSONNE_2, PERSONNE_3, PERSONNE_4) values
('oui','Formation simulation'
,'Formation par simulation en santé mentale (2015-2016)'
,'images/organi.jpg'
,'<p>Grâce à une subvention du Ministère de l’Économie et des Importations du Québec, le CRISPESH a développé un modèle de formation par simulation en santé mentale offert sur support multimédia pour les intervenants d’une ressource communautaire œuvrant en région éloignée au sein d’une communauté inuit.</p>'
,'Chargées de projet','Marise Lachapelle et Jian Wang'
,'Stagiaires - Étudiants au Cégep du Vieux-Montréal en Arts, lettres et communication, profil Médias'
,'Frédérique Lemay','Xavier Bossé','Camille Archambault');

insert into PROJET(COMPLETED,CATEGORIE, TITRE, IMAGE_P, DESCRIPTION_P, ROLE_1, PERSONNE_1, ROLE_2, PERSONNE_2,ROLE_3, PERSONNE_3, COMITE_TITRE_1,COMITE_MEMBRE_1,COMITE_TITRE_2,COMITE_MEMBRE_2,COMITE_TITRE_3,COMITE_MEMBRE_3 ) values
('oui','Applications pedagogiques'
,'Les applications pédagogiques de la conception universelle de l’apprentissage (2013-2015)'
,'images/cua.jpg'
,'<p>Ce projet a conduit à l’élaboration de stratégies pédagogiques conçues selon les principes de la conception universelle de l’apprentissage (CUA). L’objectif était de soutenir les enseignants dans la planification de cours qui répondent à la fois aux besoins des étudiants en situation de handicap et à ceux de l’ensemble des étudiants de la classe. Ce projet s’inscrivait dans une perspective proactive en proposant des solutions pédagogiques axées sur la planification plutôt que sur des réponses rétroactives à des besoins individuels.</p>
                    <p>Grâce à ce projet interordres, cinq établissements postsecondaires ont travaillé de concert: l’Université de Montréal, l’Université du Québec à Montréal (UQAM), le Collège Montmorency, le Cégep Marie-Victorin et le Cégep du Vieux Montréal.</p>
                    <p>Pour plus d’information : <a href="http://www.pcua.ca" target="_blank">www.pcua.ca</a></p>'
,'Chargée de projet, d’octobre 2013 à novembre 2014','Stéphanie Tremblay'
,'Chargé de projet, de novembre 2014 jusqu’à la fin du projet','Paul Turcotte'
,'Chargée de projet, de novembre 2014 à juin 2015','Florence Lebeau'
,'Coordination'
,'Stéphanie Tremblay
						<p>Chargée de projet, d’octobre 2013 à novembre 2014</p>

						Paul Turcotte
						<p>Chargé de projet, de novembre 2014 jusqu’à la fin du projet</p>

						Florence Lebeau
						<p>Chargée de projet, de novembre 2014 à juin 2015</p>'
,'Comité de direction'
, 'Marie Blain
						<p>Directrice adjointe aux études, Cégep Marie-Victorin</p>

						Yves Carignan
						<p>Directeur des affaires étudiantes et relations avec la communauté, Collège Montmorency, 2013-2014</p>

						Nathalie Giguère
						<p>Directrice des études, Cégep du Vieux Montréal, 2014-2015</p>

						Thomas Henderson
						<p>Directeur du Centre de recherche pour l’inclusion scolaire et professionnelle des étudiants en situation de handicap (CRISPESH), 2013-2014</p>

						Carole Lavallée
						<p>Directrice adjointe des études, Cégep du Vieux Montréal, 2013</p>

						Catherine Loiselle
						<p>Directrice générale du Centre de recherche pour l’inclusion scolaire et professionnelle des étudiants en situation de handicap (CRISPESH), 2014-2015</p>

						Dolores Otero
						<p>Directrice, Services à la vie étudiante – Centre des services d’accueil et de soutien socioéconomique, Université du Québec à Montréal</p>

						Hélène Trifiro
						<p>Directrice du Centre étudiant du soutien à la réussite, Université de Montréal</p>'
,'Comité de travail'
,'Brigitte Auclair
						<p>Enseignante  de français, Collège Montmorency </p>

						Véronique Besançon
						<p>Conseillère  pédagogique, Université de Montréal </p>

						Jean-René Corbeil
						<p>Enseignant en Technologies de l’architecture, Collège Montmorency</p>

						Antoine Coulombe
						<p>Enseignant en Techniques de travail social, Cégep Marie-Victorin</p>

						Johanne Morin
						<p>Enseignante en Techniques de travail social, Cégep Marie-Victorin </p>

						Cédric Lamathe
						<p>Enseignant de mathématiques, Cégep du Vieux Montréal</p>

						Florence Lebeau
						<p>Chargée  de cours à la Faculté de l’Aménagement, Université de Montréal </p>

						Paul Turcotte
						<p>Enseignant de philosophie, Cégep du Vieux Montréal</p>

						Steve Vezeau
						<p>Enseignant à l’École de design, Université du Québec à Montréal</p>');

-- insert into PROJET(CATEGORIE, TITRE, IMAGE_P, DESCRIPTION_P, ROLE_1, PERSONNE_1, ROLE_2, PERSONNE_2, PARTENAIRE_1, PARTENAIRE_2, PARTENAIRE_FIN_1) values
-- ('


commit;
