##
## Mehdi Monerie, Pierre L�veill�
##
##

Bonjour cher administrateur, bienvenue dans votre site web au contenu modifiable.
Premi�rement, voici les informations de connexion de l'administrateur :

Nom d'usager : admin
Mot de passe : admin

Le tout est modifiable dans le fichier "crispesh\Web\action\DAO\UserDAOhardcode.php" � la ligne 9.

Pour changer les informations de connexion pour la base de donn�es, il faut changer les constantes du fichier constants,
situ� dans le fichier "crispesh\Web\action\DAO\constants.php"


#######################################################################################################################################################

Ce qui a �t� livr� dans cette livraison :

- Accueil modifiable sur 5 champs 
- Ajouter et supprimer un membre d'�quipe
- Ajouter, supprimer, modifier une image et modifier les informations d'un projet

#######################################################################################################################################################

Dans la prochaine livraison :

- R�parer le bug apr�s le t�l�chargement de l'image (redirection vers nos_projets.php ne fonctionne pas)
- Pouvoir changer des lignes dans l'�quipe avec CKEditor
- Pouvoir modifier les informations de connexion de l'administrateur
- Am�liorer le style
- Responsive comme avant