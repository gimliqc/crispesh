<?php
	require_once("action/administrationAction.php");

	$action = new AdministrationAction();
	$action->execute();

	require_once("partial/header.php");
?>

        <div id="content">

            <div id="page-header" class="style-1">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">

                            <h4>Connexion des administrateurs</h4>

                        </div><!-- col -->
						<div class="col-sm-9">

							<ol class="breadcrumb">
                                <li><a href="index.php">Accueil</a></li>
                                <li class="active">Documentation</li>
                            </ol>

						</div><!-- col -->
                    </div><!-- row -->
                </div><!-- container -->
            </div><!-- page-header -->

		<div class="container">
			<div class="row">

		<div class="col-sm-12">
			<form action="administration.php" method="post">
				<?php
				if ($action->wrongLogin) {
					?>
					<div class="error-div"><strong>Erreur : </strong>Connexion erronée</div>
					<?php
				}
				?>
			<div class="form-label">
				<label for="username">Nom d'usager : </label>
			</div>
			<div class="form-input">
				<input type="text" name="username" id="username" />
			</div>
			<div class="form-separator"></div>

			<div class="form-label">
				<label for="password">Mot de passe : </label>
			</div>
			<div class="form-input">
				<input type="password" name="pwd" id="password" />
			</div>
			<div class="form-separator"></div>

			<div class="form-label">
				&nbsp;
			</div>
			<div class="form-input">
				<button type="submit">Connexion</button>
			</div>
			<div class="form-separator"></div>
		</form>
					</div><!-- col -->

				</div><!-- row -->
            <div class="rond_background rouge droite grosseur_5" id="cercle_2"></div>
			</div><!-- container -->

		</div><!-- CONTENT -->
<?php
    require_once("partial/footer.php");