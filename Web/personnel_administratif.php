<?php
    require_once("action/administratifAction.php");

    $action = new administratifAction();
    $action->execute();

	require_once("partial/header.php");
?>

        <div id="content">
            <div id="page-header" class="style-1">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">

                            <h4>Équipe du CRISPESH </h4>

                        </div><!-- col -->
						<div class="col-sm-8">

							<ol class="breadcrumb">
                                <li><a href="index.php">Accueil</a></li>
                                <li>Équipe du CRISPESH </li>
                            </ol>

						</div><!-- col -->
                    </div><!-- row -->
                </div><!-- container -->
            </div><!-- page-header -->

			<div class="container">
				<div class="row">

					<div class="col-sm-5">
						<div class="widget widget-categories">
							<ul>
								<li onclick="equipeBD('personnel administratif')">Personnel administratif</li>
								<li onclick="equipeBD('personnel scientifique')">Personnel scientifique</li>
								<li onclick="equipeBD('étudiants')">Étudiants (Assistants de recherche et stagiaires)</a></li>
								<li onclick="equipeBD('chercheurs')">Chercheuses et chercheurs affiliés</li>
								<li onclick="equipeBD('Conseil')">Conseil d’administration</li>
								<li><a href="emplois.php">Emplois au CRISPESH</a></li>
							</ul>
						</div><!-- widget-categories -->
					</div><!-- col -->
						<div class="col-sm-7wrap_liste_personnel"></div>
							<template id = "membre-template">
									<h6 class="nom"></h6>
									<div class="email"></div>
									<div class="telephone"></div>
									<div class="role"></div>
									<div class="role1"></div>
									<div class="role2"></div>
									<div class="emplacement1"></div>
									<div class="emplacement2"></div>
									<hr>
							</template>
					</div><!-- col -->
					<div class="rond_background bleu droite grosseur_1" id="cercle_2"></div>
				</div><!-- row -->
			</div><!-- container -->

		</div><!-- CONTENT -->

	<script src="js/js_equipe.js"></script>

        <?php
    require_once("partial/footer.php");