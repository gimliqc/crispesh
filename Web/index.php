<?php
    require_once("action/indexAction.php");

    $action = new indexAction();
    $action->execute();

	require_once("partial/header.php");
?>


        <div id="content">

            <section class="full-section" id="section-23">

				<div class="full-section-container bg-blanc">

					<div class="container">
						<div class="row">
							<div class="col-sm-12">

							<form action="index.php" method="post">
								<h1>								<?php
										if($action->isLoggedIn()){
								?>
								<textarea name="slogan" id="editor1" cols="50" rows="20" contenteditable =<?=$action->isLoggedInString()?>>
								<?php
									}
								?>
								<?php
									if($action->accueilRows[0]['SLOGAN']!=null) {  echo $action->accueilRows[0]['SLOGAN']; }

								if($action->isLoggedIn()){
									?>
								</textarea>
								<?php
								}
								?>
								</h1>
									<?php
										if($action->isLoggedIn()){
									?>
										<button type="submit">Modifier</button>
									<?php
									}
									?>
							</form>
							<script>
									CKEDITOR.replace( 'editor1', {
										customConfig: 'config.js'
									});
							</script>
							</div>
						</div>
					</div>

				</div> <!-- full-section -->

			</section>

            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
					<form action="index.php" method="post">
																<?php
										if($action->isLoggedIn()){
								?>
								<textarea name="texte" id="editor2" cols="50" rows="10" contenteditable =<?=$action->isLoggedInString()?>>
								<?php
									}
								?>
								<?php
									if($action->accueilRows[0]['TEXTE']!=null) {  echo $action->accueilRows[0]['TEXTE']; }

								if($action->isLoggedIn()){
									?>
								</textarea>
								<?php
								}
								?>

									<?php
										if($action->isLoggedIn()){
									?>
										<button type="submit">Modifier</button>
									<?php
									}
									?>
							</form>
							<script>
									CKEDITOR.replace( 'editor2', {
										customConfig: 'config.js'
									});
							</script>
                    </div><!-- col -->
                </div><!-- row -->
			</div><!-- container -->

            <div class="rond_background red droite grosseur_2" id="cercle_2"></div>

			<div class="bloc_gris">
                <div class="container">
				<div class="row">
					<div class="col-sm-12">

						<div class="headline">

						<h3>CRISPESH</h3>

						</div><!-- headline -->

					</div><!-- col -->
				</div><!-- row -->
                </div>
                <div class="clearfix"></div>
			</div><!-- container -->

			<div class="bloc_gris">
                <div class="container">
				<div class="row">
					<div class="col-sm-4">

						<div class="service-box style-2 wow">

							<i class="mt-icons-circle bleu"></i>
							<form action="index.php" method="post">
								<div class="service-box-content" >
								<h4>Notre Mission</h4>

								<?php
										if($action->isLoggedIn()){
								?>
								<textarea id="editor3" name="mission_a" cols="30" rows="10" contenteditable =<?=$action->isLoggedInString()?>>
								<?php
									}
								?>
								<?php
									if($action->accueilRows[0]['MISSION_A']!=null) {  echo $action->accueilRows[0]['MISSION_A']; }

								if($action->isLoggedIn()){
									?>
								</textarea>
								<?php
								}
								?>
								</div><!-- service-box-content -->
									<?php
										if($action->isLoggedIn()){
									?>
										<button type="submit">Modifier</button>
									<?php
									}
									?>
							</form>
							<script>
									CKEDITOR.replace( 'editor3', {
										customConfig: 'config.js'
									});
							</script>

						</div><!-- service-box -->

					</div><!-- col -->

					<div class="col-sm-4">

						<div class="service-box style-2 wow" data-wow-delay="0.4s">

							<i class="mt-icons-circle vert"></i>

							<form action="index.php" method="post">
								<div class="service-box-content" >
								<h4>Nos Valeurs</h4>

								<?php
										if($action->isLoggedIn()){
								?>
								<textarea name="values_a" id="editor4" cols="30" rows="10" contenteditable =<?=$action->isLoggedInString()?>>
								<?php
									}
								?>
								<?php
									if($action->accueilRows[0]['VALUES_A']!=null) {  echo $action->accueilRows[0]['VALUES_A']; }

								if($action->isLoggedIn()){
									?>
								</textarea>
								<?php
								}
								?>
								</div><!-- service-box-content -->
									<?php
										if($action->isLoggedIn()){
									?>
										<button type="submit">Modifier</button>
									<?php
									}
									?>
							</form>
							<script>
									CKEDITOR.replace( 'editor4', {
										customConfig: 'config.js'
									});
							</script>

						</div><!-- service-box -->

					</div><!-- col -->


					<div class="col-sm-4">

						<div class="service-box style-2 wow">

							<i class="mt-icons-circle rouge"></i>

							<div class="service-box-content">

								<form action="index.php" method="post">
								<h4>Notre Vision</h4>

								<?php
										if($action->isLoggedIn()){
								?>
								<textarea name="vision_a" id="editor5" cols="30" rows="10" contenteditable =<?=$action->isLoggedInString()?>>
								<?php
									}
								?>
								<?php
									if($action->accueilRows[0]['VISION_A']!=null) {  echo $action->accueilRows[0]['VISION_A']; }

								if($action->isLoggedIn()){
									?>
								</textarea>
								<?php
								}
								?>
								</div><!-- service-box-content -->
									<?php
										if($action->isLoggedIn()){
									?>
										<button type="submit">Modifier</button>
									<?php
									}
									?>
							</form>
							<script>

									CKEDITOR.replace( 'editor5', {
										customConfig: 'config.js'
									});
							</script>

						</div><!-- service-box -->

					</div><!-- col -->

				</div><!-- row -->
                </div>
                <div class="clearfix"></div>
			</div><!-- container -->

			<div class="container">

				<div class="rond_background bleu grosseur_2" id="cercle_1"></div>
			    <br>

				<div class="row">
					<div class="col-sm-12">

						<div class="headline">

							<h3>Le Mandat des CCTT-PSN</h3>

						</div><!-- headline -->

					</div><!-- col -->
				</div><!-- row -->

				<div class="row">
					<div class="col-sm-4">

						<div class="service-box style-2 wow">

							<i class="mt-icons-circle rouge"></i>

							<div class="service-box-content">

								<p>Soutenir les différents organismes et entreprises dans l’innovation sociale par des services d’accompagnement et de transfert de connaissances.</p>

							</div><!-- service-box-content -->

						</div><!-- service-box -->

					</div><!-- col -->
					<div class="col-sm-4">

						<div class="service-box style-2 wow">

							<i class="mt-icons-circle bleu"></i>

							<div class="service-box-content">

								<p>Développer des liens profitables entre les milieux et les collèges.</p>

							</div><!-- service-box-content -->

						</div><!-- service-box -->

					</div><!-- col -->
					<div class="col-sm-4">

						<div class="service-box style-2 wow">

							<i class="mt-icons-circle vert"></i>

							<div class="service-box-content">

								<p>Contribuer à la conception, à la réalisation et à l’amélioration des pratiques sociales novatrices au moyen de la recherche appliquée.</p>

							</div><!-- service-box-content -->

						</div><!-- service-box -->

					</div><!-- col -->
				</div><!-- row -->

				<div class="row">
					<div class="col-sm-4">

						<div class="service-box style-2 wow">

							<i class="mt-icons-circle jaune"></i>

							<div class="service-box-content">

								<p>Contribuer à former la relève scientifique en initiant les étudiants à la recherche.</p>

							</div><!-- service-box-content -->

						</div><!-- service-box -->

					</div><!-- col -->
					<div class="col-sm-4">

						<div class="service-box style-2 wow">

							<i class="mt-icons-circle mauve"></i>

							<div class="service-box-content">

								<p>Informer et former les milieux avec lesquels ils collaborent.</p>

							</div><!-- service-box-content -->

						</div><!-- service-box -->

					</div><!-- col -->

				</div><!-- row -->
			</div>

			<br>
			<hr>

			<div class="container  clearfix">

                <div class="rond_background orange droite grosseur_2" id="cercle_2"></div>
				<div class="row">
					<div class="col-sm-12">

						<div class="headline">
							<h3>Nouveaux projets en cours</h3>
						</div><!-- headline -->

					</div><!-- col -->
				</div><!-- row -->
					<div class="col-sm-4">
						<div class="service-box style-6 wow">
							<div class="service-box-image">
								<img src="<?= $action->projetAccueil[0]['IMAGE_P']?>" alt="">

							</div><!-- service-box-image -->
							<div class="service-box-content">
							<form action="projet_detail.php" method="POST" >
									<input type="hidden" id="custId" name="categorie" value="<?= $action->projetAccueil[0]['CATEGORIE'] ?>">
									<h4>
										<input type="submit" value=" <?= $action->projetAccueil[0]['TITRE'] ?>">
									</h4>
                                </form>
							</div><!-- service-box-content -->
						</div><!-- service-box -->
					</div><!-- col -->

					<div class="col-sm-4">
						<div class="service-box style-6 wow">
							<div class="service-box-image">
								<img src="<?= $action->projetAccueil[1]['IMAGE_P']?>" alt="">
							</div><!-- service-box-image -->
							<div class="service-box-content">
								<form action="projet_detail.php" method="POST" >
									<input type="hidden" id="custId" name="categorie" value="<?= $action->projetAccueil[1]['CATEGORIE'] ?>">
									<h4>
										<input type="submit" value=" <?= $action->projetAccueil[1]['TITRE'] ?>">

									</h4>
                                </form>							</div><!-- service-box-content -->
						</div><!-- service-box -->
					</div><!-- col -->

					<div class="col-sm-4">
						<div class="service-box style-6 wow">
							<div class="service-box-image">

								<img src="<?= $action->projetAccueil[2]['IMAGE_P']?>" alt="">

							</div><!-- service-box-image -->
							<div class="service-box-content">
								<form action="projet_detail.php" method="POST" >
									<input type="hidden" id="custId" name="categorie" value="<?= $action->projetAccueil[2]['CATEGORIE'] ?>">
									<h4>
										<input type="submit" value=" <?= $action->projetAccueil[2]['TITRE'] ?>">

									</h4>
                                </form>
							</div><!-- service-box-content -->
						</div><!-- service-box -->
					</div><!-- col -->

				</div><!-- row -->
			</div><!-- container -->

			<br>
			<hr>

			<section class="full-section parallax" id="section-15" data-stellar-background-ratio="0.1">

                <div class="container">

                    <div id="wrap_titre_partenaires" class="row">
                        <div class="col-md-5 border_right_verte">
                            <h3>Partenaires financiers</h3>
                            <div>

                                <img src="images/mees.png" alt="">
								<img src="images/mesi.png" alt="">

                            </div>
                        </div>
                        <div class="col-md-5 border_right_verte">
                            <h3>Cégeps affiliés</h3>
                            <div>
                                <img src="images/dawson.jpg" alt="">
                                <img src="images/vieux-montreal.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h3>Membre des réseaux</h3>
                            <div>
                                <img src="images/transtech.jpg" alt="">
								<img src="images/rqis.jpg" alt="">
								<img src="images/qc_innove.jpg" alt="">
                            </div>
                        </div>
                    </div>

		        </div><!-- container -->

			</section><!-- full-section -->

		</div><!-- CONTENT -->

<?php
    require_once("partial/footer.php");