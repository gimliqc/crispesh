<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/AccueilDAO.php");
	require_once("action/projetAction.php");

	class indexAction extends CommonAction {
		public $accueilRows;
		public $projetAccueil;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {

			if(!empty($_POST))
			foreach($_POST as $key => $value)
			{
				AccueilDAO::UpdateAccueil($key,$value);
			}

			$this->projetAccueil = ProjetsDAO::FetchAll();

			$this->accueilRows = AccueilDAO::FetchAccueil();

		}
	}
