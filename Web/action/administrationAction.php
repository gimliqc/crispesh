<?php
	require_once("action/CommonAction.php");

	require_once("action/DAO/UserDAOhardcode.php");

	class AdministrationAction extends CommonAction {
		public $wrongLogin;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {

			if (isset($_POST["username"])) {
				$visibility = UserDAOhardcode::authenticate($_POST["username"], $_POST["pwd"]);

				if ($visibility > CommonAction::$VISIBILITY_PUBLIC) {
					$_SESSION["username"] = $_POST["username"];
					$_SESSION["visibility"] = $visibility;

					header("location:index.php");
					exit;
				}
				else {
					$this->wrongLogin = true;
				}
			}
		}
	}
