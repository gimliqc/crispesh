<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/ProjetsDAO.php");

	class newProjDetAction extends CommonAction {

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {

			if(!empty($_POST)){
				if(!empty($_POST['TITRE']) && !empty($_POST['COMPLETED']) && !empty($_POST['CATEGORIE'])){
					ProjetsDao::InsertProjet($_POST);
				}
			}

		}
	}