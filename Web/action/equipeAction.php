<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/MembresDAO.php");


	class equipeAction extends CommonAction {
		private $categorie = 'personnel administratif';
		public $membres;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			if (!empty($_POST["categorie"])){
				$this->categorie=$_POST["categorie"];
				$_SESSION['categorie'] =  $_POST["categorie"];
			}

			if(!empty($_POST["supprimer"])){
				MembresDAO::DeleteMembre($_POST["EMAIL"]);
			}

			if(!empty($_POST["ajouter"]) ){
				MembresDAO::InsertMembre($_POST);

			}

			$this->membres = MembresDAO::FetchMembres($this->categorie); // liste de tout les membres de perso admin présent dans la DB

		}


	}
