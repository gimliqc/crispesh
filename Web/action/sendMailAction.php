<?php
	require_once("action/CommonAction.php");

	class sendMailAction extends CommonAction {
		private $to;
		private $subject;
		private $message;
		private $headers;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			if(!empty($_POST)){
				$this->to = "mehdimonerie@gmail.com";
				$this->subject = $_POST["subject"];
				$this->message = $_POST["message"];
				$this->headers = "From: ". $_POST["email"];

				if(strlen($this->message)>=70){
					$this->message = wordwrap($this->message,70);
				}

				mail($this->to,$this->subject,$this->message,$this->headers);
			}
		}
	}