<?php
	// session_start();

	require_once("action/DAO/constants.php");
	require_once("action/DAO/ConnectionDB.php");

	class ProjetsDAO {
		private static $connection;


		public static function FetchProjets($categ) {
			$connection = ConnectionDB::getConnection();
			$categorie=$categ;

			$statement = $connection->prepare("SELECT * FROM PROJET WHERE CATEGORIE = ?");
			$statement->bindParam(1, $categorie);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$row = $statement->fetchAll();

			return $row;
		}

		public static function FetchAll() {
			$connection = ConnectionDB::getConnection();

			$statement = $connection->prepare("SELECT * FROM PROJET");
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$row = $statement->fetchAll();

			return $row;
		}

		public static function UpdateProjet($categSQL,$key,$value) {
			try{
			$connection = ConnectionDB::getConnection();
			$statement = $connection->prepare("UPDATE PROJET SET ".$key." ='".$value."' WHERE CATEGORIE ='".$categSQL."'");
			$statement->execute();
			}catch(EXCEPTION $e)
			{
				var_dump($e);
			}
		}

		public static function InsertProjet($post){
			try{
				$keys = null;
				$values = null;

				$connection = ConnectionDB::getConnection();
				foreach($post as $key => $value){
					if($key!='submit'){
						if($key=='organisme'){
							$keys .= $key;
						}
						else{
							$keys .= $key . ",";
						}
					}
				}
				$nKeys = rtrim($keys, ',');

				foreach ($post as $key => $value) {
   					 if ($value == 'Ajouter') {
						break;
    				} else if ($value==null){
						$values .= "'',";
   					}else{
						$values .= "'".$value . "',";
					   }
				}
				$nValues = rtrim($values, ',');

				$statement = $connection->prepare("INSERT INTO PROJET (".$nKeys.")VALUES(".$nValues.")");


				$statement->execute();

				header("location:nos_projets.php");
				exit;

			}catch(EXCEPTION $e)
			{
				var_dump($e);
			}
		}

		public static function DeleteProject($categSQL) {
			$connection = ConnectionDB::getConnection();

			$statement = $connection->prepare("DELETE FROM PROJET WHERE CATEGORIE = '".$categSQL."'");
			$statement->execute();

		}


	}
