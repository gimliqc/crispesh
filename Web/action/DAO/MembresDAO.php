<?php
	// session_start();

	require_once("action/DAO/constants.php");
	require_once("action/DAO/ConnectionDB.php");

	class MembresDAO {
		private static $connection;

		public static function FetchMembres($categ) {
			$connection = ConnectionDB::getConnection();
			$categorie=$categ;

			$statement = $connection->prepare("SELECT * FROM MEMBRE WHERE CATEGORIE = ?");
			$statement->bindParam(1, $categorie);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$row = $statement->fetchAll();
			return $row;
		}


		public static function UpdateMembre($id, $categLine, $newValue) {
			$connection = ConnectionDB::getConnection();
			$categorie=$categ;

			$statement = $connection->prepare("UPDATE ? FROM MEMBRE WHERE ID = ?");
			$statement->bindParam(1,$categLine, $id);
			// $statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute([$prenom]);

			$row = $newValue->fetchAll();



		}
		public static function DeleteMembre($email) {
			$connection = ConnectionDB::getConnection();

			$statement = $connection->prepare("DELETE FROM MEMBRE WHERE EMAIL = '".$email."'");
			$statement->execute();

		}

		public static function InsertMembre($post){
			try{
				$keys = null;
				$values = null;
				$connection = ConnectionDB::getConnection();
				foreach($post as $key => $value){
					if($key!='ajouter'){
						if($key=='EMPLACEMENT_2'){
							$keys .= $key;
						}
						else{
							$keys .= $key . ",";
						}
					}
				}
				$nKeys = rtrim($keys, ',');

				foreach ($post as $key => $value) {
   					 if ($value == 'Ajouter le membre') {
						break;
    				} else if ($value==null){
						$values .= "'',";
   					}else{
						$values .= "'".$value . "',";
					   }
				}
				$nValues = rtrim($values, ',');

				$statement = $connection->prepare("INSERT INTO MEMBRE(".$nKeys.")VALUES(".$nValues.")");
				$statement->execute();

			}catch(EXCEPTION $e)
			{
				var_dump($e);
			}
		}
	}