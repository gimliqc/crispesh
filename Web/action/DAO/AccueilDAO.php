<?php
	require_once("action/DAO/constants.php");
	require_once("action/DAO/ConnectionDB.php");

	class AccueilDAO {
		private static $connection;

		public static function FetchAccueil() {
			$connection = ConnectionDB::getConnection();

			$statement = $connection->prepare("SELECT * FROM ACCUEIL");
			// $statement->bindParam(1, $email);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$row = $statement->fetchAll();
			return $row;
		}


		public static function UpdateAccueil($categLine, $newValue) {
			$connection = ConnectionDB::getConnection();
			$statement = $connection->prepare("UPDATE ACCUEIL SET ". $categLine . " = ?");
			$statement->execute([$newValue]);

		}

	}