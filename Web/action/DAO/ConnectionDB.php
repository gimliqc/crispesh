<?php
	class ConnectionDB {
		private static $connection;

		public static function getConnection() {
			// singleton!
			if (ConnectionDB::$connection == null) {
				ConnectionDB::$connection = new PDO("oci:dbname=" . DB_NAME.';charset=utf8', DB_USER, DB_PASS);
				ConnectionDB::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				ConnectionDB::$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			}

			return ConnectionDB::$connection;
		}

		// Not useful... PHP ferme la connexion automatiquement
		// public static function closeConnection() {
		// 	if (Connection::$connection != null) {
		// 		Connection::$connection = null;
		// 	}
		// }
	}