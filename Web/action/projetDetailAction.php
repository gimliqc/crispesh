<?php
	require_once("action/CommonAction.php");
	require_once("action/DAO/ProjetsDAO.php");



	class projetDetailAction extends CommonAction {
		private $categorie;
		public $projets;
		public $iterator;
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {

			if(!empty($_POST))
			{
				if (!empty($_POST["categorie"])){

					$this->categorie=$_POST["categorie"];
					$_SESSION['categorie'] =  $_POST["categorie"];
				}
			}


			if(!empty($_POST) && empty($_POST["categorie"]) && empty($_POST["supprimer"]) && empty($_POST["ajouter"])){
				foreach($_POST as $key => $value)
				{
					$this->categorie=$_SESSION['categorie'];
					ProjetsDAO::UpdateProjet($this->categorie,$key,$value);
				}
			}

			if(!empty($_POST["supprimer"]) || !empty($_POST["ajouter"])){
				foreach($_POST as $key => $value)
				{
					$this->categorie=$_SESSION['categorie'];
					if($key!="supprimer" && $key!="ajouter"){
						ProjetsDAO::UpdateProjet($this->categorie,$key,$value);
					}
				}
			}



			$this->projets = ProjetsDAO::FetchProjets($this->categorie); // liste de tout les membres de perso admin présent dans la DB

		}


	}