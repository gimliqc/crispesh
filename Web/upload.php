<?php
$target_dir = "images/";
$_FILES["fileToUpload"]["name"]=$_POST["name"];
rename ( $_FILES["fileToUpload"]["name"] , $_POST["name"] );



$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
print_r($_FILES);
print_r($_POST);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "<script>alert(\"Sorry, your file is too large.\")</script>";

    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "<script>alert(\"Sorry, only JPG, JPEG, PNG & GIF files are allowed.\")</script>";

    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        exec( "icacls " . $target_file . " /q /c /reset" );
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
        echo "<script>alert(\"Sorry, there was an error uploading your file.\")</script>";
    }
}
header("location:projet_detail.php");
?>