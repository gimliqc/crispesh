<?php
    require_once("action/equipeAction.php");

    $action = new equipeAction();
    $action->execute();

	require_once("partial/header.php");
?>

        <div id="content">

<div id="page-header" class="style-1">
	<div class="container">
		<div class="row">
			<div class="col-sm-4">

				<h4>Équipe du CRISPESH </h4>

			</div><!-- col -->
			<div class="col-sm-8">

				<ol class="breadcrumb">
					<li><a href="index.php">Accueil</a></li>
					<li>Équipe du CRISPESH </li>
					<li class="active">Personnel administratif</li>
				</ol>

			</div><!-- col -->
		</div><!-- row -->
	</div><!-- container -->
</div><!-- page-header -->

<div class="container">
	<div class="row">

		<div class="col-sm-5">
			<div class="widget widget-categories">
				<ul>


				<form action="equipe_detail.php" method="POST" >
					<input type="hidden" id="custId" name="categorie" value="personnel administratif">
					<input class="btn btn-green" type="submit" value="Personnel administratif">
				</form>
				<form action="equipe_detail.php" method="POST" >
					<input type="hidden" id="custId" name="categorie" value="personnel scientifique">
					<input class="btn btn-green" type="submit" value="Personnel scientifique">
				</form>
				<form action="equipe_detail.php" method="POST" >
					<input type="hidden" id="custId" name="categorie" value="étudiants">
					<input class="btn btn-green" type="submit" value="Étudiants (Assistants de recherche et stagiaires)">
				</form>
				<form action="equipe_detail.php" method="POST" >
					<input type="hidden" id="custId" name="categorie" value="chercheurs">
					<input class="btn btn-green" type="submit" value="Chercheuses et chercheurs affiliés">
				</form>
				<form action="equipe_detail.php" method="POST" >
					<input type="hidden" id="custId" name="categorie" value="Conseil">
					<input class="btn btn-green" type="submit" value="Conseil d’administration">
				</form>

				<a class="btn btn-green" href="emplois.php">Emplois au CRISPESH</a>
				<br>
				<?php
					if($action->isLoggedIn()){
				?>
					<a class="btn btn-blue" href="newMembre_detail.php">Ajouter un membre</a>
				<?php
				}
				?>
				</ul>

			</div><!-- widget-categories -->
		</div><!-- col -->

		<div class="col-sm-7 wrap_liste_personnel">

					<div class="col-sm-12">
					<br>
				<p><span style="color:red">* Champs requis</span></p>

				<form action="equipe_detail.php" method="post">

					Prénom : <span style="color:red">*</span><input type="text" name="PRENOM" value="">
					Nom :<span style="color:red">*</span> <input type="text" name="NOM" value="">
					Courriel :*</span> <input type="text" name="EMAIL" value="">
					Rôle actuel : <input type="text" name="ROLE_ACTUEL" value="">
					<hr>
					Département d'équipe :<br>
					<input class="radioradio" type="radio" name="categorie" value="personnel administratif" checked>Personnel administratif<br>
  					<input class="radioradio" type="radio" name="categorie" value="personnel scientifique">Personnel scientifique<br>
					<input class="radioradio" type="radio" name="categorie" value="étudiants">Étudiants (Assistants de recherche et stagiaires)<br>
					<input class="radioradio" type="radio" name="categorie" value="chercheurs">Chercheuses et chercheurs affiliés<br>
					<input class="radioradio" type="radio" name="categorie" value="Conseil">Conseil d’administration<br>
					<br>
					Téléphone : <input type="text" name="TELEPHONE" value="">
					Poste : <input type="text" name="POSTE" value="">
					Rôle passé : <input type="text" name="ROLE_1" value="">
					Lieu : <input type="text" name="EMPLACEMENT_1" value="">
					Rôle passé : <input type="text" name="ROLE_2" value="">
					Lieu : <input type="text" name="EMPLACEMENT_2" value="">


					<input class="btn btn-blue" type="submit" name="ajouter" value="Ajouter le membre">
				</form>
					</div>

				<br>

		</div><!-- col -->
		<div class="rond_background bleu droite grosseur_1" id="cercle_2"></div>
	</div><!-- row -->
</div><!-- container -->

</div><!-- CONTENT -->

<?php
require_once("partial/footer.php");