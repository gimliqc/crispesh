<?php
    require_once("action/newProjDetAction.php");

    $action = new newProjDetAction();
	$action->execute();

	require_once("partial/header.php");
?>
    <div id="content">

        <div class="container">
            <div class="row">
					<?php
						require_once("partial/newProject.php")
					?>
					<div class="col-sm-12">
					<hr>
						<a href="nos_projets.php" class="btn btn-blue btn_retour_projet">Retour à la liste des projets</a>
					</div>

                </div><!-- col -->
            </div><!-- row -->
        </div><!-- container -->

    </div><!-- CONTENT -->


    <?php
    require_once("partial/footer.php");