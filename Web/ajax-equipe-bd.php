<?php
	require_once("action/ajax/AjaxEquipeBDAction.php");

	$action = new AjaxEquipeBDAction();
	$action->execute();

	echo json_encode($action->equipe_info);