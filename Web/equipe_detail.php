<?php
    require_once("action/equipeAction.php");

    $action = new equipeAction();
    $action->execute();

	require_once("partial/header.php");
?>

        <div id="content">

<div id="page-header" class="style-1">
	<div class="container">
		<div class="row">
			<div class="col-sm-4">

				<h4>Équipe du CRISPESH </h4>

			</div><!-- col -->
			<div class="col-sm-8">

				<ol class="breadcrumb">
					<li><a href="index.php">Accueil</a></li>
					<li>Équipe du CRISPESH </li>
					<li class="active">Personnel administratif</li>
				</ol>

			</div><!-- col -->
		</div><!-- row -->
	</div><!-- container -->
</div><!-- page-header -->

<div class="container">
	<div class="row">

		<div class="col-sm-5">
			<div class="widget widget-categories">
				<ul>


				<form action="equipe_detail.php" method="POST" >
					<input type="hidden" id="custId" name="categorie" value="personnel administratif">
					<input class="btn btn-green" type="submit" value="Personnel administratif">
				</form>
				<form action="equipe_detail.php" method="POST" >
					<input type="hidden" id="custId" name="categorie" value="personnel scientifique">
					<input class="btn btn-green" type="submit" value="Personnel scientifique">
				</form>
				<form action="equipe_detail.php" method="POST" >
					<input type="hidden" id="custId" name="categorie" value="étudiants">
					<input class="btn btn-green" type="submit" value="Étudiants (Assistants de recherche et stagiaires)">
				</form>
				<form action="equipe_detail.php" method="POST" >
					<input type="hidden" id="custId" name="categorie" value="chercheurs">
					<input class="btn btn-green" type="submit" value="Chercheuses et chercheurs affiliés">
				</form>
				<form action="equipe_detail.php" method="POST" >
					<input type="hidden" id="custId" name="categorie" value="Conseil">
					<input class="btn btn-green" type="submit" value="Conseil d’administration">
				</form>

				<a class="btn btn-green" href="emplois.php">Emplois au CRISPESH</a>
				<br>
				<?php
					if($action->isLoggedIn()){
				?>
					<a class="btn btn-blue" href="newMembre_detail.php">Ajouter un membre</a>
				<?php
				}
				?>
				</ul>




			</div><!-- widget-categories -->
		</div><!-- col -->

		<div class="col-sm-7 wrap_liste_personnel">
		<?php foreach ($action->membres as $membre) {
				// foreach ($membre as $infos) ?>
					<div class="col-sm-12">
						<h6><?= $membre['PRENOM'] . " " . $membre['NOM'] ?></h6>
						<?php
							if( $membre['EMAIL']!=null){
								echo $membre['EMAIL'];
								echo "<br/>\n";
							}
							if( $membre['ROLE_ACTUEL']!=null){
								echo $membre['ROLE_ACTUEL'];
								echo "<br/>\n";
							}

							$keys = array_keys($membre);

							for($i=6; $i<sizeof($membre) ;$i++){
								if($membre[$keys[$i]]!=null){
									echo $membre[$keys[$i]];
									echo "<br/>\n";
								}
							}


						?>

					</div>
					<form action="equipe_detail.php" method="POST" onsubmit="return confirm('Êtes-vous sûr de vouloir supprimer cette personne?');">
						<input type="hidden" id="supprimer" name="supprimer" value="yes">
						<input type="hidden" id="supprimer" name="EMAIL" value="<?=$membre['EMAIL']?>">
						<?php
							if($action->isLoggedIn()){
						?>
							<input class="btn btn-red" type="submit" value="Supprimer la personne">
						<?php
						}
					?>
                    </form>
				<br>
			<?php } ?>
		</div><!-- col -->
		<div class="rond_background bleu droite grosseur_1" id="cercle_2"></div>
	</div><!-- row -->
</div><!-- container -->

</div><!-- CONTENT -->

<?php
require_once("partial/footer.php");