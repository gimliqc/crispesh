
function listMembre(data) {
	document.querySelector(".col-sm-7wrap_liste_personnel").innerHTML = "";

	let templateHTML = document.querySelector("#membre-template").innerHTML;

	for (var i = 0; i < data.length; i++) {
		let newElement = document.createElement("div");

		newElement.innerHTML = templateHTML;

		newElement.querySelector(".nom").innerHTML = data[i].PRENOM + " " + data[i].NOM;
		if (data[i].EMAIL != null)
			newElement.querySelector(".email").innerHTML =  data[i].EMAIL;
		if (data[i].TELEPHONE != null)
			newElement.querySelector(".telephone").innerHTML = data[i].TELEPHONE + " " + data[i].POSTE;
		if (data[i].ROLE_ACTUEL != null)
			newElement.querySelector(".role").innerHTML = data[i].ROLE_ACTUEL;
		if (data[i].ROLE_1 != null)
			newElement.querySelector(".role1").innerHTML = data[i].ROLE_1;
		if (data[i].EMPLACEMENT_1 != null)
			newElement.querySelector(".emplacement1").innerHTML = data[i].EMPLACEMENT_1;
		if (data[i].ROLE_2 != null)
			newElement.querySelector(".role2").innerHTML = data[i].ROLE_2
		if (data[i].EMPLACEMENT_2 != null)
			newElement.querySelector(".emplacement2").innerHTML = data[i].EMPLACEMENT_2;

		document.querySelector(".col-sm-7wrap_liste_personnel").appendChild(newElement);
	}
}



function equipeBD(categ){
	$.ajax(
		{
			type: "POST",
			url: "ajax-equipe-bd.php",
			data: {
				categorie: categ
			}
		}
	)
	.done(data => {
		data = JSON.parse(data);
		listMembre(data);
	});
}

window.onload=()=>{
	equipeBD("personnel administratif");

}