</div><!-- PAGE-WRAPPER -->

<!-- FOOTER -->
<footer class="site_footer">
    <div id="footer-bottom">

        <div class="container">
            <div class="row">
                <div class="col-sm-5">

                    <div class="widget widget-text">

                        <div>
                            <p class="last">
                                <img src="../images/logo-footer.png" alt="">
                            </p>
                        </div>

                    </div><!-- widget-text -->

                </div><!-- col -->
                <div class="col-sm-2">
                        <a href="plan_du_site.php">Plan du site</a>
                </div>
                <div class="col-sm-5">

                    <div class="widget widget-text">

                        <div>
                            <p class="copy-right">Conception Web par <a href="http://conceptionfocus.ca/" target="_blank">Conception Focus</a></p>
                        </div>

                    </div><!-- widget-text -->

                </div><!-- col -->
            </div><!-- row -->
        </div><!-- container -->

    </div><!-- footer-bottom -->

</footer><!-- FOOTER -->

<!-- GO TOP -->
<a id="go-top"><i class="mt-icons-arrow-up2"></i></a>


<!-- jQUERY -->
<!-- <script src="assets/plugins/jquery/jquery-2.1.3.min.js"></script> -->
<script src="../js/jquery-2.1.3.min.js"></script>

<!-- BOOTSTRAP JS -->
<!-- <script src="assets/bootstrap/js/bootstrap.min.js"></script> -->
<script src="../js/bootstrap.min.js"></script>

<!-- VIEWPORT -->
<!-- <script src="assets/plugins/viewport/jquery.viewport.js"></script> -->
<script src="../js/jquery.viewport.js"></script>

<!-- MENU -->
<!-- <script src="assets/plugins/menu/hoverIntent.js"></script> -->
<script src="../js/superfish.js"></script>

<!-- FANCYBOX -->
<!-- <script src="assets/plugins/fancybox/jquery.fancybox.pack.js"></script> -->
<script src="../js/jquery.fancybox.pack.js"></script>

<!-- REVOLUTION SLIDER -->
<!-- <script src="assets/plugins/revolutionslider/js/jquery.themepunch.tools.min.js"></script> -->
<script src="../js/jquery.themepunch.tools.min.js"></script>
<!-- <script src="assets/plugins/revolutionslider/js/jquery.themepunch.revolution.min.js"></script> -->
<script src="../js/jquery.themepunch.revolution.min.js"></script>

<!-- OWL Carousel -->
<!-- <script src="assets/plugins/owl-carousel/owl.carousel.min.js"></script> -->
<script src="../js/owl.carousel.min.js"></script>

<!-- PARALLAX -->
<!-- <script src="assets/plugins/parallax/jquery.stellar.min.js"></script> -->
<script src="../js/jquery.stellar.min.js"></script>

<!-- ISOTOPE -->
<!-- <script src="assets/plugins/isotope/imagesloaded.pkgd.min.js"></script> -->
<script src="../js/imagesloaded.pkgd.min.js"></script>

<!-- <script src="assets/plugins/isotope/isotope.pkgd.min.js"></script> -->
<script src="../js/isotope.pkgd.min.js"></script>


<!-- PLACEHOLDER -->
<!-- <script src="assets/plugins/placeholders/jquery.placeholder.min.js"></script> -->
<script src="../js/jquery.placeholder.min.js"></script>

<!-- CONTACT FORM VALIDATE & SUBMIT -->
<!-- <script src="assets/plugins/validate/jquery.validate.min.js"></script> -->
<script src="../js/jquery.validate.min.js"></script>

<!-- <script src="assets/plugins/submit/jquery.form.min.js"></script> -->
<script src="../js/jquery.form.min.js"></script>


<!-- GOOGLE MAPS -->
<script src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<!-- <script src="assets/plugins/googlemaps/gmap3.min.js"></script> -->
<script src="../js/gmap3.min.js"></script>

<!-- CHARTS -->
<!-- <script src="assets/plugins/charts/jquery.easypiechart.min.js"></script> -->
<script src="../js/jquery.easypiechart.min.js"></script>

<!-- COUNTER -->
<!-- <script src="assets/plugins/counter/jQuerySimpleCounter.js"></script> -->
<script src="../js/jQuerySimpleCounter.js"></script>

<!-- STATISTICS -->
<!-- <script src="assets/plugins/statistics/chart.min.js"></script> -->
<script src="../js/chart.min.js"></script>


<!-- YOUTUBE PLAYER -->
<!-- <script src="assets/plugins/ytplayer/jquery.mb.YTPlayer.js"></script> -->
<script src="../js/jquery.mb.YTPlayer.js"></script>

<!-- HOVERDIR -->
<!-- <script src="assets/plugins/hoverdir/hoverdir.js"></script> -->
<script src="../js/hoverdir.js"></script>

<!-- MAGNIFY -->
<!-- <script src="assets/plugins/magnify/jquery.magnify.js"></script> -->
<script src="../js/jquery.magnify.js"></script>

<!-- INSTAFEED -->
<!-- <script src="assets/plugins/instafeed/instafeed.min.js"></script> -->
<script src="../js/instafeed.min.js"></script>

<!-- TWITTERFETCHER -->
<!-- <script src="assets/plugins/twitterfetcher/twitterfetcher.js"></script> -->
<script src="../js/twitterfetcher.js"></script>

<!-- TEXT ROTATOR -->
<!-- <script src="assets/plugins/text-rotator/morphext.min.js"></script> -->
<script src="../js/morphext.min.js"></script>

<!-- ANIMATIONS -->
<!-- <script src="assets/plugins/animations/wow.min.js"></script> -->
<script src="../js/wow.min.js"></script>

<!-- CUSTOM JS -->
<!-- <script src="assets/js/custom.js"></script> -->
<script src="../js/custom.js"></script>
</body>

</html>