<?php
    require_once("action/projetAction.php");

    $action = new projetAction();
    $action->execute();

    require_once("partial/header.php");

?>
        <div id="content">
            <div class="headline">
                <h3>Projets en cours</h3>
                <?php
                    	if($action->isLoggedIn()){
                ?>
                <a href="newProject_detail.php" class="btn btn-blue"> NOUVEAU PROJET </a>
                <?php
                        }
                ?>
            </div>
            <?php foreach ($action->projets as $value) {
                                if($value['COMPLETED']=='non'){   ?>
            <div class="bloc_blanc bloc_projet">
                <!-- for each projet  -->
                <div class="container">

			        <div class="rond_background vert grosseur_1" id="cercle_13"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="portfolio-item">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="portfolio-item-thumbnail">
                                            <img src="<?= $value['IMAGE_P'] ?>" alt="">
                                        </div><!-- portfolio-item-thumbnail -->
                                    </div><!-- col -->
                                    <div class="col-sm-8">
                                        <div class="portfolio-item-details">
                                                <form action="projet_detail.php" method="POST" >
                                                <input type="hidden" id="custId" name="categorie" value="<?= $value['CATEGORIE'] ?>">
                                                <h4>
                                                    <a href="" type="submit"rel="bookmark" title="<?= $value['TITRE'] ?>">
                                                        <?php echo $value['TITRE'] ?>
                                                    </a>
                                                </h4>
                                                <input class="btn btn-blue" type="submit" value="Voir les détails du projet">
                                                </form>

                                                <form action="nos_projets.php" method="POST" onsubmit="return confirm('Êtes-vous sûr de vouloir supprimer ce projet?');">
                                                    <input type="hidden" id="custId" name="supprimer" value="<?= $value['CATEGORIE'] ?>">
                                                    <?php
                                                        if($action->isLoggedIn()){
                                                    ?>
                                                    <input class="btn btn-red" type="submit" value="Supprimer le projet">
                                                    <?php
                                                        }
                                                    ?>
                                                </form>


                                        </div><!-- portfolio-item-details -->
                                    </div><!-- col -->
                                </div><!-- row -->
                            </div><!-- portfolio-item -->
                        </div><!-- col -->
                    </div><!-- row -->
                </div><!-- container -->
                <?php
                    }
                }
                ?>
                <div class="headline">
                    <h3>Projets complétés</h1>
                </div>
                <?php foreach ($action->projets as $value) {
                                if($value['COMPLETED']=='oui'){   ?>
                <div class="bloc_gris bloc_projet">
                <div class="container">

			        <div class="rond_background vert grosseur_1" id="cercle_13"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="portfolio-item">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="portfolio-item-thumbnail">
                                            <img src="<?= $value['IMAGE_P'] ?>" alt="">
                                        </div><!-- portfolio-item-thumbnail -->
                                    </div><!-- col -->
                                    <div class="col-sm-8">
                                        <div class="portfolio-item-details">
                                                <form action="projet_detail.php" method="POST">
                                                    <input type="hidden" id="custId" name="categorie" value="<?= $value['CATEGORIE'] ?>">
                                                    <h4>
                                                        <a href="" type="submit"rel="bookmark" title="<?= $value['TITRE'] ?>">
                                                            <?php echo $value['TITRE'] ?>
                                                        </a>
                                                    </h4>
                                                    <input class="btn btn-blue" type="submit" value="Voir les détails du projet">
                                                </form>

                                                <form action="nos_projets.php" method="POST" onsubmit="return confirm('Êtes-vous sûr de vouloir supprimer ce projet?');">
                                                    <input type="hidden" id="custId" name="supprimer" value="<?= $value['CATEGORIE'] ?>">
                                                    <?php
                                                        if($action->isLoggedIn()){
                                                    ?>
                                                    <input class="btn btn-red" type="submit" value="Supprimer le projet">
                                                    <?php
                                                        }
                                                    ?>
                                                </form>
                                        </div><!-- portfolio-item-details -->
                                    </div><!-- col -->
                                </div><!-- row -->
                            </div><!-- portfolio-item -->
                        </div><!-- col -->
                    </div><!-- row -->
                </div><!-- container -->
        </div>
        <?php

            }
        }
        ?>




        </div><!-- CONTENT -->

<?php
    require_once("partial/footer.php");